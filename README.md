[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)

# Cooperativa - Assembleia

As operações podem ser encontradas no swagger, com seus parâmetros, mas aqui vai uma breve explicação, 
com alguns exemplos de chamadas: 

Primeiro,
```
POST http://localhost:8080/assembleia/v1/pauta

Operação para criação de uma pauta.

Body: 

{
    "descricao": "Pauta XPTO."
}
```

E então, utilizando o retorno **agendaId** na próxima chamada:

```
POST http://localhost:8080/assembleia/v1/sessao

Operação para criação de uma sessão de votação, com base numa pauta em específico.

Body: 

{
    "agendaId": "080917fb-5d4b-4613-8ef3-7c0250056237"
}
```

Após aberta a votação, os votos podem ser registrados com o seguinte endpoint:

```
POST http://localhost:8080/assembleia/v1/voto

Operação para registro dos votos.

Body: 

{
 "cpf": "03172165035",
 "agendaId": "080917fb-5d4b-4613-8ef3-7c0250056237",
 "voto": "SIM"   
}
```

E por fim, quando o prazo da votação for encerrado, será possível consultar o total de votos a favor, ou contra, 
com o seguinte endpoint:

```
GET http://localhost:8080/assembleia/v1/080917fb-5d4b-4613-8ef3-7c0250056237/sessao
```

### Running

Execute o comando `docker-compose up -d` para criação de uma instância do Postgres com a estrutura 
de tabelas utilizada para a solução do problema.

Para compilar o projeto, utilaze-se o seguinte comando maven:

```shell script
mvn clean package
```

E para execução do projeto, basta:

```shell script
mvn spring-boot:run
```

### Documentation

A documentação da API pode ser encontrada na seguinte URL:

```
http://localhost:8080/assembleia/swagger-ui.html
```

O arquivo de cobertura de testes pode ser encontrado na `target/site/jacoco/index.html`, após a execução do comando:

````shell script
mvn clean verify jacoco:report
````

### Requirements

- Maven 3.6.3+
- Java JDK 11
- Docker

### License

[MIT License](./LICENSE)
