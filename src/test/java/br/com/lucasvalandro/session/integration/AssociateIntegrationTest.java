package br.com.lucasvalandro.session.integration;

import static br.com.lucasvalandro.session.integration.response.AssociateVoteStatus.Status.ABLE_TO_VOTE;
import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;

import br.com.lucasvalandro.session.domain.HttpException;
import br.com.lucasvalandro.session.integration.response.AssociateVoteStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class AssociateIntegrationTest {

    @InjectMocks
    private AssociateIntegration integration;

    @Mock
    private RestTemplate restTemplate;

    @Before
    public void setUp() {
        ReflectionTestUtils.setField(integration, "baseUrl", "url/%s");
    }

    @Test
    public void shouldReturnErrorWhenReceiveEmptyResponse() {
        //given
        final String cpf = randomNumeric(11);
        final String url = String.format("url/%s", cpf);

        given(restTemplate.exchange(url, HttpMethod.GET, null, AssociateVoteStatus.class))
                .willReturn(ResponseEntity.of(Optional.empty()));

        //when
        catchException(() -> integration.getAssociateVoteStatus(cpf));

        //then
        final HttpException exception = caughtException();
        assertEquals(HttpStatus.BAD_GATEWAY, exception.getHttpStatus());
        assertEquals("Erro ao trabalhar como um gateway.", exception.getMessage());
    }

    @Test
    public void shouldReturnVoteStatus() {
        //given
        final String cpf = randomNumeric(11);
        final String url = String.format("url/%s", cpf);
        final AssociateVoteStatus associateVoteStatus = AssociateVoteStatus.builder()
                .status(ABLE_TO_VOTE)
                .build();

        given(restTemplate.exchange(url, HttpMethod.GET, null, AssociateVoteStatus.class))
                .willReturn(ResponseEntity.of(Optional.of(associateVoteStatus)));

        //when
        final AssociateVoteStatus result = integration.getAssociateVoteStatus(cpf);

        //then
        assertEquals(associateVoteStatus, result);
    }
}
