package br.com.lucasvalandro.session.web.v1;

import static br.com.lucasvalandro.session.web.v1.request.VoteRequest.VoteStatus.SIM;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import br.com.lucasvalandro.session.service.RegisterAgendaService;
import br.com.lucasvalandro.session.service.RegisterSessionService;
import br.com.lucasvalandro.session.service.VoteService;
import br.com.lucasvalandro.session.web.v1.request.RegisterAgendaRequest;
import br.com.lucasvalandro.session.web.v1.request.RegisterSessionRequest;
import br.com.lucasvalandro.session.web.v1.request.VoteRequest;
import br.com.lucasvalandro.session.web.v1.response.RegisterAgendaResponse;
import br.com.lucasvalandro.session.web.v1.response.VoteResultResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;
import java.time.LocalDateTime;
import java.util.Random;
import java.util.UUID;

@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class SessionResourceTest {

    private ObjectMapper mapper;
    private MockMvc mockMvc;

    @Autowired
    protected WebApplicationContext context;

    @MockBean
    private RegisterAgendaService registerAgendaService;

    @MockBean
    private RegisterSessionService registerSessionService;

    @MockBean
    private VoteService voteService;

    @Before
    public void setup() {

        mockMvc = webAppContextSetup(context)
                .build();

        mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
    }

    @Test
    public void shouldReturnBadRequestWhenRegisterAgendaReceiviesEmptyRequest() throws Exception {
        final String url = "/v1/pauta";
        final RegisterAgendaRequest request = null;

        mockMvc.perform(post(url)
                .content(mapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        verifyNoInteractions(registerAgendaService);
    }

    @Test
    public void shouldReturnBadRequestWhenRegisterAgendaReceiviesDescriptionNull() throws Exception {
        final String url = "/v1/pauta";
        final RegisterAgendaRequest request = RegisterAgendaRequest.builder().agendaDescription(null).build();

        mockMvc.perform(post(url)
                .content(mapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        verifyNoInteractions(registerAgendaService);
    }

    @Test
    public void shouldReturnBadRequestWhenRegisterAgendaReceiviesDescriptionEmpty() throws Exception {
        final String url = "/v1/pauta";
        final RegisterAgendaRequest request = RegisterAgendaRequest.builder().agendaDescription("").build();

        mockMvc.perform(post(url)
                .content(mapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        verifyNoInteractions(registerAgendaService);
    }

    @Test
    public void shouldReturnSuccessWhenRegisterAgenda() throws Exception {
        final String url = "/v1/pauta";
        final RegisterAgendaRequest request = RegisterAgendaRequest.builder()
                .agendaDescription(randomAlphabetic(10))
                .build();

        final UUID randomUUID = UUID.randomUUID();
        final RegisterAgendaResponse response = RegisterAgendaResponse.builder()
                .agendaId(randomUUID)
                .build();

        given(registerAgendaService.register(any()))
                .willReturn(response);

        mockMvc.perform(post(url)
                .content(mapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.agendaId").value(randomUUID.toString()));

        verify(registerAgendaService).register(request);
    }

    @Test
    public void shouldReturnBadRequestWhenRegisterSessionReceiviesEmptyRequest() throws Exception {
        final String url = "/v1/sessao";
        final RegisterSessionRequest request = null;

        mockMvc.perform(post(url)
                .content(mapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        verifyNoInteractions(registerSessionService);
    }

    @Test
    public void shouldReturnBadRequestWhenRegisterSessionReceiviesAgendaIdNull() throws Exception {
        final String url = "/v1/sessao";
        final RegisterSessionRequest request = RegisterSessionRequest.builder().agendaId(null).build();

        mockMvc.perform(post(url)
                .content(mapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        verifyNoInteractions(registerSessionService);
    }

    @Test
    public void shouldReturnSuccessWhenRegisterSession() throws Exception {
        final String url = "/v1/sessao";
        final UUID randomUUID = UUID.randomUUID();
        final RegisterSessionRequest request = RegisterSessionRequest.builder()
                .agendaId(randomUUID)
                .endSessionDateTime(LocalDateTime.now())
                .build();

        doNothing()
            .when(registerSessionService)
            .register(any());

        mockMvc.perform(post(url)
                .content(mapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        verify(registerSessionService).register(request);
    }

    @Test
    public void shouldReturnBadRequestWhenRegisterVoteReceiviesEmptyRequest() throws Exception {
        final String url = "/v1/voto";
        final VoteRequest request = null;

        mockMvc.perform(post(url)
                .content(mapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        verifyNoInteractions(voteService);
    }

    @Test
    public void shouldReturnBadRequestWhenRegisterVoteReceiviesInvalidCpf() throws Exception {
        final String url = "/v1/voto";
        final String randomNumeric = randomNumeric(1);
        final VoteRequest request = VoteRequest.builder().cpf(randomNumeric.repeat(11)).build();

        mockMvc.perform(post(url)
                .content(mapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        verifyNoInteractions(voteService);
    }

    @Test
    public void shouldReturnBadRequestWhenRegisterVoteReceiviesAgendaIdNull() throws Exception {
        final String url = "/v1/voto";
        final VoteRequest request = VoteRequest.builder()
                .cpf("17587596585")
                .voteStatus(SIM)
                .build();

        mockMvc.perform(post(url)
                .content(mapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        verifyNoInteractions(voteService);
    }

    @Test
    public void shouldReturnSuccessWhenRegisterVote() throws Exception {
        final String url = "/v1/voto";
        final VoteRequest request = VoteRequest.builder()
                .cpf("17587596585")
                .agendaId(UUID.randomUUID())
                .voteStatus(SIM)
                .build();

        doNothing()
                .when(voteService)
                .register(any());

        mockMvc.perform(post(url)
                .content(mapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        verify(voteService).register(request);
    }

    @Test
    public void shouldReturnBadRequestWhenGetResultsReceivesAgendaIdIsMalformed() throws Exception {
        final String url = "/v1/%s/sessao";
        final String agendaId = randomAlphabetic(5);

        final VoteResultResponse response = VoteResultResponse.builder()
                .no(new Random().nextInt())
                .yes(new Random().nextInt())
                .build();

        given(voteService.getResult(any()))
                .willReturn(response);

        mockMvc.perform(get(String.format(url, agendaId)))
                .andExpect(status().isBadRequest());

        verifyNoInteractions(voteService);
    }

    @Test
    public void shouldReturnSuccessWhenGetResults() throws Exception {
        final String url = "/v1/%s/sessao";
        final UUID agendaId = UUID.randomUUID();

        final VoteResultResponse response = VoteResultResponse.builder()
                .no(new Random().nextInt())
                .yes(new Random().nextInt())
                .build();

        given(voteService.getResult(any()))
                .willReturn(response);

        mockMvc.perform(get(String.format(url, agendaId)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.sim").value(response.getYes()))
                .andExpect(jsonPath("$.nao").value(response.getNo()));

        verify(voteService).getResult(agendaId);
    }
}
