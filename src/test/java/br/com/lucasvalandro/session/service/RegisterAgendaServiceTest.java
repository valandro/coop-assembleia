package br.com.lucasvalandro.session.service;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;

import br.com.lucasvalandro.session.domain.HttpException;
import br.com.lucasvalandro.session.entity.AgendaEntity;
import br.com.lucasvalandro.session.repository.AgendaRepository;
import br.com.lucasvalandro.session.web.v1.request.RegisterAgendaRequest;
import br.com.lucasvalandro.session.web.v1.response.RegisterAgendaResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class RegisterAgendaServiceTest {

    @InjectMocks
    private RegisterAgendaService service;

    @Mock
    private AgendaRepository repository;

    @Captor
    private ArgumentCaptor<AgendaEntity> agendaCaptor;

    @Test
    public void shouldReturnExceptionWhenAgendaAlreadyExists() {
        //given
        final String agendaDescription = randomAlphabetic(10);
        final RegisterAgendaRequest request = RegisterAgendaRequest.builder()
                .agendaDescription(agendaDescription)
                .build();

        given(repository.findByDescriptionEquals(agendaDescription))
                .willReturn(Optional.of(new AgendaEntity()));

        //when
        catchException(() -> service.register(request));

        //then
        final HttpException exception = caughtException();
        assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, exception.getHttpStatus());
        assertEquals("Pauta já cadastrada no sistema.", exception.getMessage());
    }

    @Test
    public void shouldSaveNewAgenda() {
        //given
        final String agendaDescription = randomAlphabetic(10);
        final RegisterAgendaRequest request = RegisterAgendaRequest.builder()
                .agendaDescription(agendaDescription)
                .build();

        given(repository.findByDescriptionEquals(agendaDescription))
                .willReturn(Optional.empty());

        given(repository.save(agendaCaptor.capture()))
                .willReturn(new AgendaEntity());

        //when
        final RegisterAgendaResponse result = service.register(request);

        //then
        final AgendaEntity actualEntity = agendaCaptor.getValue();

        assertEquals(actualEntity.getAgendaId(), result.getAgendaId());
        assertEquals(actualEntity.getDescription(), request.getAgendaDescription());
    }
}
