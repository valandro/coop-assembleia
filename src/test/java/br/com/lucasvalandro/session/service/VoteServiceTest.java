package br.com.lucasvalandro.session.service;

import static br.com.lucasvalandro.session.integration.response.AssociateVoteStatus.Status.ABLE_TO_VOTE;
import static br.com.lucasvalandro.session.integration.response.AssociateVoteStatus.Status.UNABLE_TO_VOTE;
import static br.com.lucasvalandro.session.web.v1.request.VoteRequest.VoteStatus.SIM;
import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;

import br.com.lucasvalandro.session.domain.HttpException;
import br.com.lucasvalandro.session.entity.AgendaEntity;
import br.com.lucasvalandro.session.entity.ElectionEntity;
import br.com.lucasvalandro.session.entity.SessionEntity;
import br.com.lucasvalandro.session.entity.VoteHistoricEntity;
import br.com.lucasvalandro.session.integration.AssociateIntegration;
import br.com.lucasvalandro.session.integration.response.AssociateVoteStatus;
import br.com.lucasvalandro.session.repository.AgendaRepository;
import br.com.lucasvalandro.session.repository.ElectionRepository;
import br.com.lucasvalandro.session.repository.VoteHistoricRepository;
import br.com.lucasvalandro.session.web.v1.request.VoteRequest;
import br.com.lucasvalandro.session.web.v1.response.VoteResultResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;

@RunWith(MockitoJUnitRunner.class)
public class VoteServiceTest {

    @InjectMocks
    private VoteService service;

    @Mock
    private ElectionRepository electionRepository;

    @Mock
    private AgendaRepository agendaRepository;

    @Mock
    private VoteHistoricRepository voteHistoricRepository;

    @Mock
    private AssociateIntegration associateIntegration;

    @Test
    public void shouldReturnErrorWhenRegisterVoteAndAgendaNotFound() {
        //given
        final VoteRequest request = mockRequest();

        given(agendaRepository.findByAgendaIdEquals(any()))
                .willReturn(Optional.empty());

        //when
        catchException(() -> service.register(request));

        //then
        final HttpException exception = caughtException();
        assertEquals(HttpStatus.NOT_FOUND, exception.getHttpStatus());
        assertEquals("Sessão não encontrada.", exception.getMessage());

        verify(agendaRepository).findByAgendaIdEquals(request.getAgendaId());
        verifyNoInteractions(electionRepository, voteHistoricRepository, associateIntegration);
    }

    @Test
    public void shouldReturnErrorWhenRegisterVoteAndSessionNotFound() {
        //given
        final VoteRequest request = mockRequest();

        final AgendaEntity agendaEntity = new AgendaEntity();

        given(agendaRepository.findByAgendaIdEquals(any()))
                .willReturn(Optional.of(agendaEntity));

        //when
        catchException(() -> service.register(request));

        //then
        final HttpException exception = caughtException();
        assertEquals(HttpStatus.NOT_FOUND, exception.getHttpStatus());
        assertEquals("Sessão não encontrada.", exception.getMessage());

        verify(agendaRepository).findByAgendaIdEquals(request.getAgendaId());
        verifyNoInteractions(electionRepository, voteHistoricRepository, associateIntegration);
    }
    @Test
    public void shouldReturnErrorWhenRegisterVoteAndSessionAlreadyFinished() {
        //given
        final VoteRequest request = mockRequest();

        final AgendaEntity agendaEntity = new AgendaEntity();
        final SessionEntity sessionEntity = new SessionEntity();
        sessionEntity.setEndOn(LocalDateTime.now().minusMinutes(1));
        agendaEntity.setSession(sessionEntity);

        given(agendaRepository.findByAgendaIdEquals(any()))
                .willReturn(Optional.of(agendaEntity));

        //when
        catchException(() -> service.register(request));

        //then
        final HttpException exception = caughtException();
        assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, exception.getHttpStatus());
        assertEquals("Sessão já finalizada.", exception.getMessage());

        verify(agendaRepository).findByAgendaIdEquals(request.getAgendaId());
        verifyNoInteractions(electionRepository, voteHistoricRepository, associateIntegration);
    }

    @Test
    public void shouldReturnErrorWhenRegisterVoteAndAssociateUnableToVote() {
        //given
        final VoteRequest request = mockRequest();

        final AgendaEntity agendaEntity = new AgendaEntity();
        final SessionEntity sessionEntity = new SessionEntity();
        sessionEntity.setEndOn(LocalDateTime.now().plusMinutes(1));
        agendaEntity.setSession(sessionEntity);

        given(agendaRepository.findByAgendaIdEquals(any()))
                .willReturn(Optional.of(agendaEntity));

        final AssociateVoteStatus associateVoteStatus = AssociateVoteStatus.builder()
                .status(UNABLE_TO_VOTE)
                .build();

        given(associateIntegration.getAssociateVoteStatus(any()))
                .willReturn(associateVoteStatus);

        //when
        catchException(() -> service.register(request));

        //then
        final HttpException exception = caughtException();
        assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, exception.getHttpStatus());
        assertEquals("O cooperado não pode votar.", exception.getMessage());

        verify(agendaRepository).findByAgendaIdEquals(request.getAgendaId());
        verify(associateIntegration).getAssociateVoteStatus(request.getCpf());
        verifyNoInteractions(electionRepository, voteHistoricRepository);
    }

    @Test
    public void shouldReturnErrorWhenRegisterVoteAndAssociateAlreadyVoted() {
        //given
        final VoteRequest request = mockRequest();

        final AgendaEntity agendaEntity = new AgendaEntity();
        final SessionEntity sessionEntity = new SessionEntity();
        sessionEntity.setEndOn(LocalDateTime.now().plusMinutes(1));
        agendaEntity.setSession(sessionEntity);

        given(agendaRepository.findByAgendaIdEquals(any()))
                .willReturn(Optional.of(agendaEntity));

        final AssociateVoteStatus associateVoteStatus = AssociateVoteStatus.builder()
                .status(ABLE_TO_VOTE)
                .build();

        given(associateIntegration.getAssociateVoteStatus(any()))
                .willReturn(associateVoteStatus);

        given(voteHistoricRepository.findByCpfEquals(any()))
                .willReturn(Optional.of(new VoteHistoricEntity()));

        //when
        catchException(() -> service.register(request));

        //then
        final HttpException exception = caughtException();
        assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, exception.getHttpStatus());
        assertEquals("O cooperado não pode votar.", exception.getMessage());

        verify(agendaRepository).findByAgendaIdEquals(request.getAgendaId());
        verify(associateIntegration).getAssociateVoteStatus(request.getCpf());
        verify(voteHistoricRepository).findByCpfEquals(request.getCpf());

        verifyNoInteractions(electionRepository);
    }

    @Test
    public void shouldRegisterNewVote() {
        //given
        final VoteRequest request = mockRequest();

        final AgendaEntity agendaEntity = new AgendaEntity();
        final SessionEntity sessionEntity = new SessionEntity();
        sessionEntity.setEndOn(LocalDateTime.now().plusMinutes(1));
        agendaEntity.setSession(sessionEntity);

        given(agendaRepository.findByAgendaIdEquals(any()))
                .willReturn(Optional.of(agendaEntity));

        final AssociateVoteStatus associateVoteStatus = AssociateVoteStatus.builder()
                .status(ABLE_TO_VOTE)
                .build();

        given(associateIntegration.getAssociateVoteStatus(any()))
                .willReturn(associateVoteStatus);

        given(voteHistoricRepository.findByCpfEquals(any()))
                .willReturn(Optional.empty());

        //when
        service.register(request);

        //then
        verify(agendaRepository).findByAgendaIdEquals(request.getAgendaId());
        verify(associateIntegration).getAssociateVoteStatus(request.getCpf());
        verify(voteHistoricRepository).findByCpfEquals(request.getCpf());
    }

    @Test
    public void shouldReturnErrorWhenGetResultsAndAgendaNotFound() {
        //given
        final UUID agendaId = UUID.randomUUID();

        given(agendaRepository.findByAgendaIdEquals(any()))
                .willReturn(Optional.empty());

        //when
        catchException(() -> service.getResult(agendaId));

        //then
        final HttpException exception = caughtException();
        assertEquals(HttpStatus.NOT_FOUND, exception.getHttpStatus());
        assertEquals("Sessão não encontrada.", exception.getMessage());

        verify(agendaRepository).findByAgendaIdEquals(agendaId);
    }

    @Test
    public void shouldReturnErrorWhenGetResultsAndSessionNotFound() {
        //given
        final UUID agendaId = UUID.randomUUID();

        final AgendaEntity agendaEntity = new AgendaEntity();

        given(agendaRepository.findByAgendaIdEquals(any()))
                .willReturn(Optional.of(agendaEntity));

        //when
        catchException(() -> service.getResult(agendaId));

        //then
        final HttpException exception = caughtException();
        assertEquals(HttpStatus.NOT_FOUND, exception.getHttpStatus());
        assertEquals("Sessão não encontrada.", exception.getMessage());

        verify(agendaRepository).findByAgendaIdEquals(agendaId);

    }

    @Test
    public void shouldReturnErrorWhenGetResultsAndSessionNotFinishedYet() {
        //given
        final UUID agendaId = UUID.randomUUID();

        final AgendaEntity agendaEntity = new AgendaEntity();
        final SessionEntity sessionEntity = new SessionEntity();
        sessionEntity.setEndOn(LocalDateTime.now().plusMinutes(1));
        agendaEntity.setSession(sessionEntity);

        given(agendaRepository.findByAgendaIdEquals(any()))
                .willReturn(Optional.of(agendaEntity));

        //when
        catchException(() -> service.getResult(agendaId));

        //then
        final HttpException exception = caughtException();
        assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, exception.getHttpStatus());
        assertEquals("Sessão não finalizada.", exception.getMessage());

        verify(agendaRepository).findByAgendaIdEquals(agendaId);
    }

    @Test
    public void shouldReturnErrorWhenGetResultsAndElectionNotFound() {
        //given
        final UUID agendaId = UUID.randomUUID();

        final AgendaEntity agendaEntity = new AgendaEntity();
        final SessionEntity sessionEntity = new SessionEntity();

        sessionEntity.setEndOn(LocalDateTime.now().minusMinutes(1));
        agendaEntity.setSession(sessionEntity);

        given(agendaRepository.findByAgendaIdEquals(any()))
                .willReturn(Optional.of(agendaEntity));

        //when
        catchException(() -> service.getResult(agendaId));

        //then
        final HttpException exception = caughtException();
        assertEquals(HttpStatus.NOT_FOUND, exception.getHttpStatus());
        assertEquals("Votação não encontrada.", exception.getMessage());

        verify(agendaRepository).findByAgendaIdEquals(agendaId);
    }

    @Test
    public void shouldReturnResults() {
        //given
        final UUID agendaId = UUID.randomUUID();

        final AgendaEntity agendaEntity = new AgendaEntity();
        final SessionEntity sessionEntity = new SessionEntity();
        final ElectionEntity electionEntity = new ElectionEntity();
        electionEntity.setNo(new Random().nextInt());
        electionEntity.setYes(new Random().nextInt());

        sessionEntity.setEndOn(LocalDateTime.now().minusMinutes(1));
        sessionEntity.setElection(electionEntity);
        agendaEntity.setSession(sessionEntity);

        given(agendaRepository.findByAgendaIdEquals(any()))
                .willReturn(Optional.of(agendaEntity));

        //when
        final VoteResultResponse result = service.getResult(agendaId);

        //then
        assertEquals(result.getNo(), electionEntity.getNo());
        assertEquals(result.getYes(), electionEntity.getYes());

        verify(agendaRepository).findByAgendaIdEquals(agendaId);
    }

    private VoteRequest mockRequest() {
        return VoteRequest.builder()
                .agendaId(UUID.randomUUID())
                .cpf(randomNumeric(11))
                .voteStatus(SIM)
                .build();
    }
}
