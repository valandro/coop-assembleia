package br.com.lucasvalandro.session.service;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import br.com.lucasvalandro.session.domain.HttpException;
import br.com.lucasvalandro.session.entity.AgendaEntity;
import br.com.lucasvalandro.session.entity.SessionEntity;
import br.com.lucasvalandro.session.repository.AgendaRepository;
import br.com.lucasvalandro.session.repository.SessionRepository;
import br.com.lucasvalandro.session.repository.VoteHistoricRepository;
import br.com.lucasvalandro.session.web.v1.request.RegisterSessionRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import java.util.Optional;
import java.util.UUID;

@RunWith(MockitoJUnitRunner.class)
public class RegisterSessionServiceTest {

    @InjectMocks
    private RegisterSessionService service;

    @Mock
    private AgendaRepository agendaRepository;

    @Mock
    private SessionRepository sessionRepository;

    @Captor
    private ArgumentCaptor<SessionEntity> sessionCaptor;

    @Test
    public void shouldReturnAgendaNotFound() {
        //given
        final RegisterSessionRequest request = RegisterSessionRequest.builder()
                .agendaId(UUID.randomUUID())
                .build();

        given(agendaRepository.findByAgendaIdEquals(any()))
                .willReturn(Optional.empty());

        //when
        catchException(() -> service.register(request));

        //then
        final HttpException exception = caughtException();
        assertEquals(HttpStatus.NOT_FOUND, exception.getHttpStatus());
        assertEquals("Pauta não encontrada.", exception.getMessage());

        verify(agendaRepository).findByAgendaIdEquals(request.getAgendaId());
    }

    @Test
    public void shouldReturnSessionAlreadyRegistered() {
        //given
        final RegisterSessionRequest request = RegisterSessionRequest.builder()
                .agendaId(UUID.randomUUID())
                .build();

        final AgendaEntity agendaEntity = new AgendaEntity();
        final SessionEntity sessionEntity = new SessionEntity();
        agendaEntity.setSession(sessionEntity);

        given(agendaRepository.findByAgendaIdEquals(any()))
                .willReturn(Optional.of(agendaEntity));

        //when
        catchException(() -> service.register(request));

        //then
        final HttpException exception = caughtException();
        assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, exception.getHttpStatus());
        assertEquals("Sessão já cadastrada no sistema.", exception.getMessage());

        verify(agendaRepository).findByAgendaIdEquals(request.getAgendaId());
    }

    @Test
    public void shouldSaveNewSession() {
        //given
        final RegisterSessionRequest request = RegisterSessionRequest.builder()
                .agendaId(UUID.randomUUID())
                .build();

        final AgendaEntity agendaEntity = new AgendaEntity();
        given(agendaRepository.findByAgendaIdEquals(any()))
                .willReturn(Optional.of(agendaEntity));

        given(sessionRepository.save(sessionCaptor.capture()))
                .willReturn(new SessionEntity());

        //when
        service.register(request);

        //then
        final SessionEntity actualEntity = sessionCaptor.getValue();

        assertEquals(actualEntity.getAgenda(), agendaEntity);

        verify(agendaRepository).findByAgendaIdEquals(request.getAgendaId());
    }
}
