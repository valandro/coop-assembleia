package br.com.lucasvalandro.session.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HttpException extends RuntimeException {
    private HttpStatus httpStatus;
    private String message;
}
