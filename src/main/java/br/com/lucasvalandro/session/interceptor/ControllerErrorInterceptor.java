package br.com.lucasvalandro.session.interceptor;

import br.com.lucasvalandro.session.domain.ErrorInfo;
import br.com.lucasvalandro.session.domain.HttpException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice
public class ControllerErrorInterceptor {

    @ExceptionHandler(HttpException.class)
    protected ResponseEntity<Object> handleEntityNotFound(final HttpException ex) {
        final ErrorInfo errorInfo = new ErrorInfo(ex.getHttpStatus().value(), ex.getMessage());
        return ResponseEntity.status(ex.getHttpStatus()).body(errorInfo);
    }
}
