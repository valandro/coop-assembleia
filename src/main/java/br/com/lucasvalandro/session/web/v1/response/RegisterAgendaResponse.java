package br.com.lucasvalandro.session.web.v1.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import java.util.UUID;

@Builder
@Getter
@Setter
public class RegisterAgendaResponse {

    @ApiModelProperty(value = "Identificador da pauta cadastrada.")
    private final UUID agendaId;
}
