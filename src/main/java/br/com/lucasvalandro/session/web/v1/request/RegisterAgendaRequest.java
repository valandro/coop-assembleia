package br.com.lucasvalandro.session.web.v1.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import javax.validation.constraints.NotBlank;

@Builder
@Data
public class RegisterAgendaRequest {

    @ApiModelProperty(value = "Descrição da pauta que será cadastrada.", required = true)
    @NotBlank(message = "Descrição da pauta não deve ser nula.")
    @JsonProperty(value = "descricao")
    private final String agendaDescription;
}
