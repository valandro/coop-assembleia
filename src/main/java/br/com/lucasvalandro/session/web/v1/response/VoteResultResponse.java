package br.com.lucasvalandro.session.web.v1.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class VoteResultResponse {
    @JsonProperty(value = "sim")
    private final Integer yes;

    @JsonProperty(value = "nao")
    private final Integer no;
}
