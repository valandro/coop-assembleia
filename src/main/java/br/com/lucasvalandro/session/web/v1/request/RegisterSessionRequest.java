package br.com.lucasvalandro.session.web.v1.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.UUID;

@Builder
@Data
public class RegisterSessionRequest {

    @ApiModelProperty(value = "Identificador da pauta que terá a sessão iniciada.", required = true)
    @NotNull(message = "Identificador da pauta não deve ser nulo.")
    private final UUID agendaId;

    @ApiModelProperty(value = "Data hora fim da sessão.")
    @JsonProperty(value = "dataHoraFim")
    private final LocalDateTime endSessionDateTime;
}
