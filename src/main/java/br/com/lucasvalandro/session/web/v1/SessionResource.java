package br.com.lucasvalandro.session.web.v1;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import br.com.lucasvalandro.session.service.RegisterAgendaService;
import br.com.lucasvalandro.session.service.RegisterSessionService;
import br.com.lucasvalandro.session.service.VoteService;
import br.com.lucasvalandro.session.web.v1.request.RegisterAgendaRequest;
import br.com.lucasvalandro.session.web.v1.request.RegisterSessionRequest;
import br.com.lucasvalandro.session.web.v1.request.VoteRequest;
import br.com.lucasvalandro.session.web.v1.response.RegisterAgendaResponse;
import br.com.lucasvalandro.session.web.v1.response.VoteResultResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/v1")
public class SessionResource implements SessionResourceApiDefinition {

    @Autowired
    private RegisterAgendaService registerAgendaService;

    @Autowired
    private RegisterSessionService registerSessionService;

    @Autowired
    private VoteService voteService;

    @Override
    @PostMapping(value = "/pauta", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public RegisterAgendaResponse registerAgenda(@Valid @RequestBody final RegisterAgendaRequest request) {
        return registerAgendaService.register(request);
    }

    @Override
    @PostMapping(value = "/sessao")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void registerSession(@Valid @RequestBody RegisterSessionRequest request) {
        registerSessionService.register(request);
    }

    @Override
    @PostMapping(value = "/voto")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void voteAgenda(@Valid @RequestBody VoteRequest request) {
        voteService.register(request);
    }

    @Override
    @GetMapping(value = "/{agendaId}/sessao", produces = APPLICATION_JSON_VALUE)
    public VoteResultResponse getResult(@PathVariable("agendaId") final UUID agendaId) {
        return voteService.getResult(agendaId);
    }
}
