package br.com.lucasvalandro.session.web.v1;

import br.com.lucasvalandro.session.web.v1.request.RegisterAgendaRequest;
import br.com.lucasvalandro.session.web.v1.request.RegisterSessionRequest;
import br.com.lucasvalandro.session.web.v1.request.VoteRequest;
import br.com.lucasvalandro.session.web.v1.response.RegisterAgendaResponse;
import br.com.lucasvalandro.session.web.v1.response.VoteResultResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.UUID;

@Api(tags = "session-resource")
public interface SessionResourceApiDefinition {

    @ApiOperation(value = "Recurso para cadastro de uma nova pauta para a assembleia.",
                  notes = "Após o cadastro da pauta, seu identificador único é retornado pela API.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Nova pauta cadastrada com sucesso."),
            @ApiResponse(code = 400, message = "Corpo da requisição inválido."),
            @ApiResponse(code = 422, message = "Pauta já cadastrada no sistema."),
            @ApiResponse(code = 500, message = "Erro interno do sistema.")
    })
    public RegisterAgendaResponse registerAgenda(
            @ApiParam(value = "Request contendo descrição da pauta.", required = true) RegisterAgendaRequest request);

    @ApiOperation(value = "Recurso para abertura de uma nova sessão para a determinada pauta.",
            notes = "Após o cadastro da pauta, seu identificador único pode ser usado para inicar uma nova sessão.")
    @ApiResponses({
            @ApiResponse(code = 204, message = "Sessão criada com sucesso."),
            @ApiResponse(code = 400, message = "Corpo da requisição inválido."),
            @ApiResponse(code = 422, message = "Sessão já cadastrada no sistema."),
            @ApiResponse(code = 404, message = "Pauta não encontrada no sistema."),
            @ApiResponse(code = 500, message = "Erro interno do sistema.")
    })
    public void registerSession(
            @ApiParam(value = "Request contendo id associado a pauta e possível data hora do fim da sessão.", required = true) RegisterSessionRequest request);


    @ApiOperation(value = "Recurso para registro de um novo voto para determinada pauta.",
            notes = "Após a abertura da sessão, novos votos podem ser cadastrados utilizando um CPF e o identificador da pauta.")
    @ApiResponses({
            @ApiResponse(code = 204, message = "Voto criado com sucesso."),
            @ApiResponse(code = 400, message = "Corpo da requisição inválido."),
            @ApiResponse(code = 404, message = "Sessão não encontrada."),
            @ApiResponse(code = 422, message = "Sessão já finalizada."),
            @ApiResponse(code = 422, message = "O cooperado não pode votar."),
            @ApiResponse(code = 500, message = "Erro interno do sistema."),
            @ApiResponse(code = 502, message = "Erro ao trabalhar como um gateway.")
    })
    public void voteAgenda(@ApiParam(value = "Request contendo id associado a pauta e CPF do cooperado.", required = true) VoteRequest request);


    @ApiOperation(value = "Recurso para consulta do resultado de uma sessão de votos.",
            notes = "Após a abertura da sessão, novos votos podem ser cadastrados utilizando um CPF e o identificador da pauta.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Resultados retornados com sucesso."),
            @ApiResponse(code = 400, message = "Corpo da requisição inválido."),
            @ApiResponse(code = 404, message = "Sessão não encontrada."),
            @ApiResponse(code = 404, message = "Votação não encontrada."),
            @ApiResponse(code = 422, message = "Sessão não finalizada."),
            @ApiResponse(code = 500, message = "Erro interno do sistema.")
    })
    public VoteResultResponse getResult(@ApiParam(value = "Request contendo id associado a pauta.", required = true) final UUID agendaId);
}
