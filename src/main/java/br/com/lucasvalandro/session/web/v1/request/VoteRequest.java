package br.com.lucasvalandro.session.web.v1.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import org.hibernate.validator.constraints.br.CPF;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Builder
@Data
public class VoteRequest {

    @ApiModelProperty(value = "CPF do cooperado.", required = true)
    @CPF
    private final String cpf;

    @ApiModelProperty(value = "Identificador da pauta que terá a sessão iniciada.", required = true)
    @NotNull(message = "Identificador da pauta não deve ser nulo.")
    private final UUID agendaId;

    @ApiModelProperty(value = "Voto do cooperado para a pauta em questão.", required = true)
    @JsonProperty(value = "voto")
    @NotNull
    private VoteStatus voteStatus;

    @Getter
    public enum VoteStatus {
        SIM, NAO;
    }
}
