package br.com.lucasvalandro.session.entity;

import static javax.persistence.EnumType.STRING;

import lombok.Data;
import lombok.Getter;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "VOTE_HISTORIC")
public class VoteHistoricEntity {
    private static final String SEQUENCE = "VOTE_HISTORIC_SEQUENCE";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQUENCE)
    @SequenceGenerator(name = SEQUENCE, sequenceName = SEQUENCE, allocationSize = 1)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "cpf", nullable = false)
    private String cpf;

    @Enumerated(STRING)
    @Column(name = "vote", nullable = false)
    private VoteStatus vote;

    @Getter
    public enum VoteStatus {
        YES, NO;
    }
}
