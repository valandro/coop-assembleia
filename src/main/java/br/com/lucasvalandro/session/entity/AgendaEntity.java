package br.com.lucasvalandro.session.entity;

import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Data
@Table(name = "AGENDA")
public class AgendaEntity {

    private static final String SEQUENCE = "AGENDA_SEQUENCE";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQUENCE)
    @SequenceGenerator(name = SEQUENCE, sequenceName = SEQUENCE, allocationSize = 1)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "agenda_id", nullable = false)
    private UUID agendaId;

    @Column(name = "description", nullable = false)
    private String description;

    @OneToOne(mappedBy = "agenda")
    private SessionEntity session;
}
