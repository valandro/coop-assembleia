package br.com.lucasvalandro.session.service;

import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY;

import br.com.lucasvalandro.session.domain.HttpException;
import br.com.lucasvalandro.session.entity.AgendaEntity;
import br.com.lucasvalandro.session.entity.SessionEntity;
import br.com.lucasvalandro.session.repository.AgendaRepository;
import br.com.lucasvalandro.session.repository.SessionRepository;
import br.com.lucasvalandro.session.web.v1.request.RegisterSessionRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Optional;

@Service
@Slf4j
public class RegisterSessionService {

    @Autowired
    private AgendaRepository agendaRepository;

    @Autowired
    private SessionRepository sessionRepository;

    @Transactional
    public void register(final RegisterSessionRequest request) {
        final LocalDateTime endOn = Optional.ofNullable(request.getEndSessionDateTime())
                                            .orElse(LocalDateTime.now().plusMinutes(1));

        final AgendaEntity agendaEntity =
                agendaRepository.findByAgendaIdEquals(request.getAgendaId())
                                .orElseThrow(() -> new HttpException(NOT_FOUND, "Pauta não encontrada."));

        Optional.ofNullable(agendaEntity.getSession())
                         .ifPresentOrElse(e -> {
                             throw new HttpException(UNPROCESSABLE_ENTITY, "Sessão já cadastrada no sistema.");
                         }, () -> {
                             final SessionEntity sessionEntity = new SessionEntity();
                             sessionEntity.setAgenda(agendaEntity);
                             sessionEntity.setEndOn(endOn);

                             sessionRepository.save(sessionEntity);
                         });
    }
}
