package br.com.lucasvalandro.session.service;

import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY;

import br.com.lucasvalandro.session.domain.HttpException;
import br.com.lucasvalandro.session.entity.AgendaEntity;
import br.com.lucasvalandro.session.repository.AgendaRepository;
import br.com.lucasvalandro.session.web.v1.request.RegisterAgendaRequest;
import br.com.lucasvalandro.session.web.v1.response.RegisterAgendaResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Optional;
import java.util.UUID;

@Service
@Slf4j
public class RegisterAgendaService {

    @Autowired
    private AgendaRepository repository;

    public RegisterAgendaResponse register(final RegisterAgendaRequest request) {
        final Optional<AgendaEntity> entity = repository.findByDescriptionEquals(request.getAgendaDescription());
        final UUID agendaId = UUID.randomUUID();

        entity.ifPresentOrElse(e -> {
            throw new HttpException(UNPROCESSABLE_ENTITY, "Pauta já cadastrada no sistema.");
        }, () -> {
            final AgendaEntity agendaEntity = new AgendaEntity();
            agendaEntity.setDescription(request.getAgendaDescription());
            agendaEntity.setAgendaId(agendaId);

            repository.save(agendaEntity);
        });

        return RegisterAgendaResponse.builder()
                .agendaId(agendaId)
                .build();
    }
}
