package br.com.lucasvalandro.session.service;

import static br.com.lucasvalandro.session.entity.VoteHistoricEntity.VoteStatus.NO;
import static br.com.lucasvalandro.session.entity.VoteHistoricEntity.VoteStatus.YES;
import static br.com.lucasvalandro.session.integration.response.AssociateVoteStatus.Status.UNABLE_TO_VOTE;
import static br.com.lucasvalandro.session.web.v1.request.VoteRequest.VoteStatus.SIM;
import static java.util.Objects.isNull;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY;

import br.com.lucasvalandro.session.domain.HttpException;
import br.com.lucasvalandro.session.entity.AgendaEntity;
import br.com.lucasvalandro.session.entity.ElectionEntity;
import br.com.lucasvalandro.session.entity.SessionEntity;
import br.com.lucasvalandro.session.entity.VoteHistoricEntity;
import br.com.lucasvalandro.session.integration.AssociateIntegration;
import br.com.lucasvalandro.session.integration.response.AssociateVoteStatus;
import br.com.lucasvalandro.session.repository.AgendaRepository;
import br.com.lucasvalandro.session.repository.ElectionRepository;
import br.com.lucasvalandro.session.repository.VoteHistoricRepository;
import br.com.lucasvalandro.session.web.v1.request.VoteRequest;
import br.com.lucasvalandro.session.web.v1.response.VoteResultResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

@Service
@Slf4j
public class VoteService {

    @Autowired
    private ElectionRepository electionRepository;

    @Autowired
    private AgendaRepository agendaRepository;

    @Autowired
    private VoteHistoricRepository voteHistoricRepository;

    @Autowired
    private AssociateIntegration associateIntegration;

    @Transactional
    public void register(final VoteRequest request) {
        final AgendaEntity agendaEntity =
                agendaRepository.findByAgendaIdEquals(request.getAgendaId())
                                .orElseThrow(() -> new HttpException(NOT_FOUND, "Sessão não encontrada."));

        final SessionEntity sessionEntity =
                Optional.ofNullable(agendaEntity.getSession())
                        .orElseThrow(() -> new HttpException(NOT_FOUND, "Sessão não encontrada."));

        if (LocalDateTime.now().isAfter(sessionEntity.getEndOn())) {
            throw new HttpException(UNPROCESSABLE_ENTITY, "Sessão já finalizada.");
        }

        final AssociateVoteStatus voteStatus = associateIntegration.getAssociateVoteStatus(request.getCpf());

        if (voteStatus.getStatus() == UNABLE_TO_VOTE) {
            throw new HttpException(UNPROCESSABLE_ENTITY, "O cooperado não pode votar.");
        }

        voteHistoricRepository.findByCpfEquals(request.getCpf())
                .ifPresentOrElse(e -> {
                    throw new HttpException(UNPROCESSABLE_ENTITY, "O cooperado não pode votar.");
                }, () -> {
                    final VoteHistoricEntity voteHistoricEntity = new VoteHistoricEntity();

                    voteHistoricEntity.setCpf(request.getCpf());
                    voteHistoricEntity.setVote((request.getVoteStatus() == SIM) ? YES : NO);

                    voteHistoricRepository.save(voteHistoricEntity);
                });

        final ElectionEntity electionEntity = Optional.ofNullable(sessionEntity.getElection())
                                                      .orElseGet(ElectionEntity::new);

        final int yes = (isNull(electionEntity.getYes())) ? 0 : electionEntity.getYes();
        final int no = (isNull(electionEntity.getNo())) ? 0 : electionEntity.getNo();

        if (request.getVoteStatus() == SIM) {
            electionEntity.setYes(yes + 1);
            electionEntity.setNo(no);
        } else {
            electionEntity.setYes(yes);
            electionEntity.setNo(no + 1);
        }

        electionEntity.setSession(sessionEntity);

        electionRepository.save(electionEntity);
    }

    public VoteResultResponse getResult(final UUID agendaId) {
        final AgendaEntity agendaEntity =
                agendaRepository.findByAgendaIdEquals(agendaId)
                        .orElseThrow(() -> new HttpException(NOT_FOUND, "Sessão não encontrada."));

        final SessionEntity sessionEntity =
                Optional.ofNullable(agendaEntity.getSession())
                        .orElseThrow(() -> new HttpException(NOT_FOUND, "Sessão não encontrada."));

        if (LocalDateTime.now().isBefore(sessionEntity.getEndOn())) {
            throw new HttpException(UNPROCESSABLE_ENTITY, "Sessão não finalizada.");
        }

        return Optional.ofNullable(agendaEntity.getSession())
                .map(SessionEntity::getElection)
                .map(electionEntity ->
                        VoteResultResponse.builder()
                                .yes(electionEntity.getYes())
                                .no(electionEntity.getNo())
                                .build())
                .orElseThrow(() -> new HttpException(NOT_FOUND, "Votação não encontrada."));
    }
}
