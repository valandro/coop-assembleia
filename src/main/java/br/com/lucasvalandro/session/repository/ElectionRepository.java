package br.com.lucasvalandro.session.repository;

import br.com.lucasvalandro.session.entity.ElectionEntity;
import br.com.lucasvalandro.session.entity.SessionEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface ElectionRepository extends CrudRepository<ElectionEntity, Long> {

    Optional<ElectionEntity> findBySessionIdEquals(final SessionEntity sessionId);
}
