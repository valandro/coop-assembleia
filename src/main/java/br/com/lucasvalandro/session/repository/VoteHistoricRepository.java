package br.com.lucasvalandro.session.repository;

import br.com.lucasvalandro.session.entity.VoteHistoricEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface VoteHistoricRepository extends CrudRepository<VoteHistoricEntity, Long> {

    Optional<VoteHistoricEntity> findByCpfEquals(final String cpf);
}
