package br.com.lucasvalandro.session.repository;

import br.com.lucasvalandro.session.entity.AgendaEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface AgendaRepository extends CrudRepository<AgendaEntity, Long> {

    Optional<AgendaEntity> findByDescriptionEquals(final String description);

    Optional<AgendaEntity> findByAgendaIdEquals(final UUID agendaId);
}
