package br.com.lucasvalandro.session.repository;

import br.com.lucasvalandro.session.entity.AgendaEntity;
import br.com.lucasvalandro.session.entity.SessionEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface SessionRepository extends CrudRepository<SessionEntity, Long> {

    Optional<SessionEntity> findByAgendaIdEquals(final AgendaEntity agendaId);
}
