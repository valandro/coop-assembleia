package br.com.lucasvalandro.session.integration.response;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;

@Data
@Builder
public class AssociateVoteStatus {

    private final Status status;

    @Getter
    public enum Status {
        ABLE_TO_VOTE, UNABLE_TO_VOTE;
    }
}
