package br.com.lucasvalandro.session.integration;


import static org.springframework.http.HttpStatus.BAD_GATEWAY;

import br.com.lucasvalandro.session.domain.HttpException;
import br.com.lucasvalandro.session.integration.response.AssociateVoteStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import java.util.Optional;

@Component
@Slf4j
public class AssociateIntegration {

    @Value("${urls.api}/%s")
    private String baseUrl;

    @Autowired
    private RestTemplate restTemplate;

    public AssociateVoteStatus getAssociateVoteStatus(final String cpf) {
        final String url = String.format(baseUrl, cpf);

        return Optional.of(restTemplate.exchange(url, HttpMethod.GET, null, AssociateVoteStatus.class))
                       .map(HttpEntity::getBody)
                       .orElseThrow(() -> new HttpException(BAD_GATEWAY, "Erro ao trabalhar como um gateway."));
    }
}
